module PartyManager

open System
open FsToolkit.ErrorHandling

type Party =
    { Name: string
      Id: Guid
      HostIp: Uri
      Password: string option }

type PartyDTO =
    { Name: string
      HostIp: string
      Password: string option }

let mutable parties: Party list = []

let updateParties newList = parties <- newList

let addParty (party: Party) =
    let partyAlreadyExist =
        parties
        |> List.tryFind (fun x -> x.HostIp = party.HostIp)
        |> Option.isSome

    match partyAlreadyExist with
    | true -> Result.Error "Host ip already exist"
    | false ->
        parties
        |> List.append [ party ]
        |> updateParties

        Result.Ok "Party registered"


let removeParty partyId =
    let targetParty =
        parties
        |> List.tryFind (fun party -> party.Id = partyId)

    match targetParty with
    | Some party ->
        parties
        |> List.except [ party ]
        |> updateParties

        Result.Ok "Party unregistered"
    | None ->
        Result.Error "Party not found"