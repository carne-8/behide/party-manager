module Json

open Thoth.Json.Net
open System.IO
open Newtonsoft.Json.Linq
open Newtonsoft.Json
open FsToolkit.ErrorHandling
open Thoth.Json.Net.Cache

let Encoders = lazy Cache<BoxedEncoder>()
let Decoders = lazy Cache<BoxedDecoder>()

let inline encoder<'T> = Encode.Auto.generateEncoderCached<'T>(Encoders.Value, DateTimeStrategy.AsString, UnionStrategy.NameInArray, CaseStrategy.CamelCase)
let inline decoder<'T> = Decode.Auto.generateDecoderCached<'T>(Decoders.Value, caseStrategy = CamelCase)

let fromStream<'T> (stream:Stream) =
    use streamReader = new StreamReader(stream)
    use jsonReader = new JsonTextReader(streamReader)
    let json = JToken.LoadAsync(jsonReader)
    json
    |> Task.map (Decode.fromValue "$" (decoder<'T>))

let toString<'T> (data: 'T) =
    data
    |> encoder
    |> Encode.toString 0

let fromString<'T> (data: string) =
    data
    |> Decode.fromString decoder<'T>