[<AutoOpen>]
module Common

open Giraffe
open System.Threading.Tasks
open Microsoft.AspNetCore.Http


module RequestErrors =
    let UNAUTHORIZED : HttpHandler =
        RequestErrors.UNAUTHORIZED "Bearer" "" [| |]

module Response =
    let noContent : HttpFunc =
        Successful.NO_CONTENT earlyReturn

    let notFound : HttpFunc =
        (   [| |]
            |> setBody
            |> RequestErrors.notFound  ) earlyReturn

    let internalError err : HttpFunc =
        ServerErrors.INTERNAL_ERROR (err |> string) earlyReturn

    let forbidden : HttpFunc =
        (   [| |]
            |> setBody
            |> RequestErrors.forbidden ) earlyReturn

    let badRequest err : HttpFunc =
        RequestErrors.BAD_REQUEST err earlyReturn

    let redirectTo (permanent : bool) (location : string) : HttpFunc =
        redirectTo permanent location earlyReturn

    let completeTR ctx (tr: Task<Result<HttpFunc, HttpFunc>>) : HttpFuncResult =
        task {
            match! tr with
            | Result.Ok chain -> return! chain ctx
            | Result.Error func -> return! func ctx
        }

    let unauthorized : HttpFunc =
        RequestErrors.UNAUTHORIZED earlyReturn

    let json data : HttpFunc =
        Giraffe.Core.json data earlyReturn


[<AutoOpen>]
module HttpHandler =

    let handleContextTR (handler: HttpContext -> Task<Result<HttpFunc, HttpFunc>>) : HttpHandler =
        handleContext (fun ctx ->
            task {
                match! handler ctx with
                | Result.Ok res -> return! res ctx
                | Result.Error res -> return! res ctx
            }
        )