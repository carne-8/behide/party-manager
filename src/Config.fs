module Config

open Microsoft.Extensions.Configuration

let envVar =
    ConfigurationBuilder()
        .AddEnvironmentVariables()
        .Build()

let apiAuthToken = "Bearer " + envVar["BEHIDE_PARTY_MANAGER_API_TOKEN"]