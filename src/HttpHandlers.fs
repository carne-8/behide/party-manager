module HttpHandlers

open System
open System.Threading.Tasks
open FsToolkit.ErrorHandling
open Microsoft.AspNetCore.Http
open Giraffe

let requireAuth : HttpHandler =
    authorizeRequest
        (fun ctx ->
            match ctx.TryGetRequestHeader "Authorization" with
            | Some token -> Config.apiAuthToken = token
            | None -> false)
        (RequestErrors.UNAUTHORIZED "Bearer" "" "Not authorized.")

module PartyManager =
    let registerGame: HttpHandler =
        handleContextTR (fun ctx ->
            taskResult {
                let! (rawParty: PartyManager.PartyDTO) =
                    ctx.Request.Body
                    |> Json.fromStream
                    |> TaskResult.mapError Response.badRequest

                let! hostIp =
                    match Uri.TryCreate(rawParty.HostIp, UriKind.Absolute) with
                    | true, uri -> Result.Ok uri
                    | _ -> Result.Error (Response.badRequest "Unable to parse host ip")

                let party: PartyManager.Party =
                    { Name = rawParty.Name
                      Id = Guid.NewGuid()
                      HostIp = hostIp
                      Password = rawParty.Password }

                return!
                    party
                    |> PartyManager.addParty
                    |> Result.map Response.json
                    |> Result.mapError Response.badRequest
            }
        )

    let unregisterGame (partyId: Guid) : HttpHandler =
        handleContextTR (fun _ctx ->
            taskResult {
                return!
                    partyId
                    |> PartyManager.removeParty
                    |> Result.map Response.json
                    |> Result.mapError Response.badRequest
            }
        )

    let getGames (next: HttpFunc) (ctx: HttpContext) = json PartyManager.parties next ctx

    let partyManagerRoutes: HttpHandler =
        choose [
            requireAuth >=> choose [
                POST >=> subRouteCi "/register-party" registerGame
                DELETE >=> subRoutef "/unregister-party/%O" unregisterGame
                GET >=> subRouteCi "/parties" getGames
            ]
        ]