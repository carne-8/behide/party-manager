FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /app

COPY Behide-PartyManager.sln .
COPY src ./src

RUN dotnet publish src/Behide-PartyManager.fsproj -c Release -o out

FROM mcr.microsoft.com/dotnet/aspnet:6.0
COPY --from=build /app/out .
EXPOSE 80
ENTRYPOINT ["dotnet", "Behide-PartyManager.dll"]